<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'files', 'action' => 'index'));
	Router::connect('/admin',array('controller'=>'files','action'=>'index', 'admin' => true));
	Router::connect('/upload',array('controller'=>'files','action'=>'upload'));
	Router::connect('/edit/:ref',array('controller'=>'files','action'=>'edit'), array('pass' => array('ref')));
	Router::connect('/show/:ref',array('controller'=>'files','action'=>'show'), array('pass' => array('ref')));
	Router::connect('/download/:ref-:fd',array('controller'=>'files','action'=>'download'), array('pass' => array('ref', 'fd'), 'id' => '*', 'fd' => '1|0'));
	Router::connect('/about',array('controller'=>'pages','action'=>'about'));

	Router::connect('/users/activate/:id-:token', array('controller' => 'users', 'action' => 'activate'), array('pass' => array('id', 'token'), 'id' => '[0-9]+', 'token' => '[a-z0-9\-]+'));
	Router::connect('/users/password/:id-:timestamp-:token', array('controller' => 'users', 'action' => 'forgot_password_login'), array('pass' => array('id', 'timestamp', 'token'), 'id' => '[0-9]+', 'timestamp' => '[0-9]+', 'token' => '[a-z0-9\-]+'));

	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
